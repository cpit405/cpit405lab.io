---
title: "Syllabus"
date: 2024-01-27T09:12:23+03:00
draft: false
---

**Credits:** 3 credit hours
**Prerequisite:** CPIT-370 and CPIT-252

## Description

This course introduces the principles and techniques of web application development. Students will engage in a hands-on exploration of modern web technologies and acquire the skills to build interactive and dynamic web applications. Students will learn the foundational infrastructure of the web, the HTTP protocol, and client-side programming concepts (HTML, CSS, JavaScript, DOM API, and Ajax) and building efficient component-based web development using React.js. This course covers the fundamental concepts of server-side scripting with PHP and how to interact with databases and build RESTful API endpoints to facilitate communication between web applications and external services.


## Course Learning Outcomes
By completion of the course the students should be able to:

1. Describe the essential concepts associated with internet architecture that supports web applications.
2. Understand the basic structure of the World-Wide-Web.
3. Identify the compatibility issues between the well-known browsers.
4. Use HTML5 markup tags for structuring web pages.
5. Use HTML5 with appropriate CSS properties and elements for styling, formatting, and enhancing web pages.
6. Construct and validate web pages using HTML5 and
CSS3.
7. Implement client-side application logic using JavaScript.
8. Handle event using JavaScript and DOM in client-side.
9. Create and use extensible markup language.
10. Create JSON in JavaScript and insert JSON data into HTML.
11. Implement Server-Side script to serve client-side requests.
12. Develop dynamic web pages using Ajax technology.

## Textbook/References
- There is no course textbook. Here are some general resources on web developments:
  - [Mozilla Developer Network (MDN) Web Docs](https://developer.mozilla.org)
  - [LearnJavaScript](https://learnjavascript.online)
  - [Eloquent JavaScript, 3rd edition (2018)](https://eloquentjavascript.net/)
  - [Web Dev](https://web.dev/)


## Topics
- The Internet and its Architecture
- HTML
- Cascading Style Sheets
- JavaScript: Part 1 (Introduction, Control Statements & Functions)
- JavaScript: Part 2 (Arrays & Objects)
- Document Object Model (DOM)
- JavaScript: Part 3 (Events)
- JavaScript Object Notation (JSON)
- React
- PHP and MySQL


## Grading
- Labs: 15%
- Assignments/in-class activities: 10%
- Group Project: 25%
- Midterm exam: 20%
- Final exam: 30%

## Attendance policy

Students are expected to attend all regularly scheduled class meetings and adhere to KAU's attendance policy, the [undergraduate study and examination bylaw (article 9) PDF ↗ 📁](https://admission.kau.edu.sa/Files/210/Files/162684_Undergraduate_Study_and_Examination_Bylaw.pdf). This policy prohibits students from having more than 25% of absences without a reasonable excuse. Any student who exceeds this limit will receive a "Denied" grade (DN) and will be disqualified from taking the final exam. This means for a 15-week semester:
- Classes held twice a week (MW), you are not allowed to miss more than 8 classes.
- Classes held three times a week (STU), you are not allowed to miss more than 12 classes.

## Late Submission Policy
Assignments submitted late will incur a 25% penalty. You may submit an assignment or a lab activity up to one week late. After that the submission will not be graded and you’ll receive 0 points for it. However, every student gets three free late passes, allowing you to submit a maximum of 3 assignments up to 1 week past the due date without penalty.

## Missed Exam Policy

If you find out that you are unable to attend the midterm or final exam, you will need to get in touch with me as soon as possible and provide necessary documentation, so we can make other arrangements with the approval of the department.

## Academic Integrity

As a student, you are expected to submit your own original work. You may not submit work written by others (human, tools, or generative AI) and claim it to be yours, or use "recycle" work prepared for previous courses without obtaining written permission from your instructor. Violations of academic integrity include, but are not limited to, cheating, plagiarism, falsifying data, knowingly assisting others in acts of academic dishonesty. Students who commit offenses of academic integrity will be reported to the deanship of academic affairs and will face disciplinary actions as per article 4 of KAU's Students’ Behavior Control Regulations (code of conduct). These actions could result in outcomes such as failure on the assignment or exam, failure in the course, suspension for one semester, or even expulsion from the university. KAU's code of conduct can be [downloaded using this link](https://www.kau.edu.sa/GetFile.aspx?id=313706&fn=Students%E2%80%99+Behavior+Control+Regulation+at+KAU+(code+of+conduct).pdf).

## Policy on the use of generative AI tools

This policy pertains to the use of generative Artificial Intelligence (AI) tools that are also called Large Language Models (LLMs) such as ChatGPT, Bard, GitHub CoPilot, Bing Chat, Claude and many others. It's important to note that this course emphasizes the acquisition of essential knowledge and the development of technical skills. Thus, it's expected that all coursework and assessments should be prepared by the student working individually or in groups as specified. **Students may not have another person or AI tools complete any substantive portion of the coursework or assessment.** Academic integrity is a core principle in academia and you're expected to uphold this principle. This applies to all work, regardless of whether or not you have used generative AI tools. However, you're allowed to experiment with these tools as long as they support your own learning, and they don't become shortcuts to dishonest work. A responsible use of generative AI tools in assigned course work or assessment must be approached ethically and in accordance with the following:
 
#### Unacceptable uses of generative AI tools
- Do not feed direct assignment/coursework questions into these tools and obtain the answers.
- Do not use these tools to generate content or solutions (words or source code) to direct questions in an assignment or any other coursework submission. This includes copying and pasting, or paraphrasing the answers produced by these tools.
- You also should not use these tools to generate summaries and rely upon them as a substitute for the original course materials.

#### Acceptable uses of generative AI tools
You may use AI tools to: 
- Explore a topic, find examples, definitions, limitations, etc.
- Brainstorm project/presentations ideas
- Create individual study guides
- Fix your grammar and style
- Proofread, paraphrase, or refine your original writing
- Code refactoring: refactor your original code.
- Debug and fix a bug introduced in your code

Unless granted a permission by the course instructor, any use of generative AI tools outside of acceptable uses in this course is considered a violation of academic integrity and will be subject to KAU's disciplinary actions.

#### Cite your use of generative AI tools
The acceptable use of generative AI tools must be cited properly just as in any other source. You're required to acknowledge your use of generative AI tools. For example, if you use ChatGPT-3, you must cite at the end of your work: *ChatGPT-3, DD/MM/YYYY* of the prompt*, "Full text of your prompt". Please keep the following in mind when using generative AI tools:
- If you copy verbatim from a generative AI tool, you must cite it with double quotation marks, indicating that the words used were not your own. Otherwise, you're considered cheating.
- If you paraphrase the output of a generative AI tool, you must cite it but not necessarily with double quotation marks, indicating that the idea, approach, format presented were not originally your own. Changing only a few words without citation is always considered plagiarism because the original ideas or data were presented without proper acknowledgment.

#### Check for correctness and accuracy
Generative AI tools are trained on imperfect data and are known for generating biased or factually incorrect output. You must not trust anything that these tools generate. If you have used them to explore a topic, and they returned a fact, date, or a number, assume it is wrong unless you either know the answer or can verify it with another source.

#### AI Detection

You must be transparent in how you used generative AI tools. If your work is suspected of not being original work, your course instructor may ask you to demonstrate your own understanding. This includes requiring you to do an oral presentation and asking you questions to demonstrate your authorship. Your course instructor may also use AI-detection tools provided by the university (e.g., Turnitin) that will generate an AI creation probability score, and if your work is flagged at a rate higher than 20% and you failed to demonstrate your authorship, then it's considered a violation of academic integrity and you will be subject to KAU's disciplinary actions.

## Students and Disability Accommodations
KAU welcomes students with disabilities into all of the University’s educational programs. If you are a student with special needs, your course instructor will work with the Special Needs Center Services at KAU's Deanship of Students' Affairs to provide you with reasonable and appropriate accommodations. This should be communicated as early in the semester as possible as the process of providing you with accommodations require administrative work.

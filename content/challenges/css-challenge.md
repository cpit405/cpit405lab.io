---
title: "CSS Challenge"
date: 2023-09-17T21:51:26+03:00
draft: false
---

[when2meet](https://when2meet.com) is a free scheduling tool that helps people find the best time to schedule a meeting. In this CSS challenge, we're going to create a clone of _when2meet_ called _"let's meet"_.
In a group of two students, create a web page using HTML and CSS that looks like the following prototype:

![Prototype](/images/challenges/lets-meet-prototype.svg)

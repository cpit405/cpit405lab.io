---
title: "Miscellaneous"
date: 2022-01-26T11:59:35+03:00
draft: false
---

This webpage contains a list of useful tools and resources you may need in web development.


### Text Editors
You'll need a text editor to manage all the code you write. Below is a list of widely used text editors:
- [Visual Studio Code (VS Code)](https://code.visualstudio.com/) an excellent option for Mac, Windows and Linux.
- [Adobe Brackets]()
- [Notepad++](https://notepad-plus-plus.org/). it’s only available on Windows. 
- There's also [Atom](https://atom.io/) for all platforms.
- For PHP, you may use a text editor with a plugin or an IDE. If you prefer to use an IDE, there're many options:
  - [PhpStorm](https://www.jetbrains.com/phpstorm/): It's a complete IDE but it requires a license (paid subscription). However, students can get it for free when signing up with their school email address.
  - [Apache NetBeans](https://netbeans.apache.org/): Free and open source IDE.
  - [Eclipse](https://www.eclipse.org/): Free and open source IDE.

### Web Browser

You'll also need a web browser. Popular browsers include Chrome, Firefox, Safari, Microsoft Edge, Brave or Opera. You will need the browser's web development tools to inspect and debug your code.

## Local Web Serve
Setting up a local web server on your machine allows you to test your code on your local machine without publishing anything to the internet (e.g., purchasing a static IP and domain name, and hosting it on a VM).
If you're using VS Code as your text editor, you can download an extension called "Live Server". This allows you to run and start a local development server with live reload feature.


## Public URL for your local app
Instead of purchasing a domain, you can use [ngrock &#x2197;](https://ngrok.com/) to obtain a public URL for your local web server.

## Other tools
- [Emmet &#x2197;](https://emmet.io): A plug-in for text editors that greatly improve your productivity by using abbreviations to allow fast coding and editing workflow. [See the complete cheatsheet &#x2197;](https://docs.emmet.io/cheat-sheet/)
- [JSFiddle &#x2197;](https://jsfiddle.net/) is an online IDE service for testing and showcasing HTML, CSS and JavaScript code snippets.
- [CodePen &#x2197;](https://codepen.io/) is an online IDE service for testing and showcasing HTML, CSS and JavaScript code snippets.

## Hosting a static website on GitHub

[GitHub Pages](https://pages.github.com/) is a static site hosting service that serves HTML, CSS, and JavaScript files directly from a repository on GitHub.


Below is how to host a static website on GitHub Pages.

1. Sign up or sign in on GitHub and create a new repository.
  ![gh-pages-step-01](/images/notes/gh-pages-step-01.png)
2. Fill out the create repository form and click **"Create Repository"**
![gh-pages step 02](/images/notes/gh-pages-step-02.png)
3. Create a new file
![gh-pages step 03](/images/notes/gh-pages-step-03.png)
4. Name the new file as `index.html` and write your HTML code. To save your code, write a commit message and click **"Commit new file"**.
![gh-pages step 04](/images/notes/gh-pages-step-04.png)
5. Click on the repository settings.
![gh-pages step 05](/images/notes/gh-pages-step-05.png)
6. Click on the **"Pages"** tab.
![gh-pages step 06](/images/notes/gh-pages-step-06.png)
7. Click on the drop-down menu and select the **main** branch.
![gh-pages step 07](/images/notes/gh-pages-step-07.png)
8. Click **"Save"**
![gh-pages step 08](images/notes/gh-pages-step-08.png)

Open up a browser and go to https://**username**.github.io/**repository**. Replace username with your GitHub username and repository with the name of the repository you have created.
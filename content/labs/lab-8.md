---
title: "Lab 8"
date: 2023-11-01T16:39:11+03:00
draft: false
---


# React.js

## Objective

-  Understand and use React fundamentals:
  - State management: Utilize the `useState` hook to manage the app's state
- Event handling: Implement event listeners to handle user interactions
- Routing: Employ React Router to implement navigation between different components

## Lab Description

Develop a React application that enables users to shorten long URLs into concise and shareable links. The application should have the following features:
- URL Shortening: Takes a long URL and returns a shortened link.
- Custom short URLs: Allow users to create custom shortened URLs, making them more customizable.
- Create an About us page and use React router to navigate into it.

![Link Shrinker App](/images/labs/lab-8/react-lab-8.png)



### Submission

- Please submit a link to your project on CodeSandbox.io. 
  - Create a project on [CodeSandbox.io](https://codesandbox.io/), be sure it is set to public, click on share, and copy link.

<span id="page2" class="anchor"></span>



---
title: "Lab 1: Setting up your web development environment"
date: 2023-09-03T10:00:36+03:00
draft: false
---
In this lab, we will go through the steps to set up your local development environment. We'll install essential development tools such as Visual Studio Code and the Live Server extension. This will enable you to launch your own websites locally. You'll also learn how to use ngrok, a tunneling tool that allows you to access your local development environment from anywhere. We will also install Postman and use it to interact with APIs. 

> Get ready to code, launch, and share your work!


## Video

{{< youtube id="ZguU2wYp-BA?rel=0" title="Lab 1: Setting up your web development" >}}




## Objective

- Set up a web development environment.
- Install and set up Visual Studio Code as your primary web development editor.
- Understand and utilize the Live Server extension for launching local web servers from VS Code.
- Install and configure ngrok to expose your local server to the internet for testing and sharing.
- Discover the basics of Postman and its role in sending HTTP requests and interacting with APIs.
- Create a GitHub account and establish your online portfolio.
- Deploy your first website using GitHub Pages to share and host your work.




## Lab Requirements

- Code Editor: [VS Code](https://code.visualstudio.com/). Also, install the [live server](https://marketplace.visualstudio.com/items?itemName=ritwickdey.LiveServer) 
- Version Control: [install git for Windows](https://git-scm.com/download/win)
- Sign up for a free [GitHub](https://github.com) account
- Sign up for a free [ngrock](https://ngrock.com) account
- Install [ngrock from the dashboard](https://dashboard.ngrok.com/)


# Lab Activities

## 1. Install Visual Studio (VS) Code
- Install [VS Code](https://code.visualstudio.com/) and create your first HTML code.
  - Create a new file. Name it `index.html`
- Install the [live server VS code extension](https://marketplace.visualstudio.com/items?itemName=ritwickdey.LiveServer).
- Add Your Personal Header. Within the `<body>` section, create a header that showcases your name:

```html
<h1>Your Name</h1>
```
- Right click and select *"open with live server"*.
- It should start a local web server on port 5500 or similar and serve the page and open it in your default web browser.
- Take a screenshot


## 2. Deploy your first webpage into GitHub Pages
GitHub Pages is a static site hosting service that takes HTML, CSS, and JavaScript files directly from a repository on GitHub, optionally runs the files through a build process, and publishes a website. 
You'll use GitHub pages to host your website directly from your GitHub repository at no additional cost.

### Install git
- Windows:
  - [install git for Windows](https://git-scm.com/download/win)
- macOS:
  - Install Apple's Command Line Developer Tools: `xcode-select --install`
  - Install Git with either [Homebrew](https://brew.sh/): `brew install git` or with [MacPorts](https://www.macports.org): `sudo port install git`

### Create and publish your first webpage on GitHub pages

- Create a repo named *cpit-405-lab-1*
- Upload or push your *index.html* file there.
- Enable GitHub Pages.
- Visit the webpage at the hosted URL.
- Take a screenshot

## 3. Install ngrok
Sometimes, we want to demo websites quickly without deploying them or hosting them on a server. _ngrok_ ngrok allocates you a hostname and port in their server and securely tunnel your local web server to the internet using a publicly accessible domain. It 
creates a secure/encrypted connection between the public endpoint and your local machine.

- Sign up for a free account on [ngrok with GitHub](https://ngrok.com/).
- Download and install [ngrok](https://ngrok.com/)
  - **Windows**: Download a standalone executable 
  - **macOS**: Download a standalone executable  or install it using [Homebrew](https://brew.sh/): `brew install ngrok`.
- Follow the installation instructions to configure it:
```shell
ngrok config add-authtoken YOUR_TOKEN_HERE
```

- Put your web app online at a domain forwarding to your local port 5500, by running:
 and run: `ngrok http 5500`
- Visit the local web page from your smart phone.
- Take a screenshot


## 4. Install Postman
Postman is a popular API client that makes it easy for developers to use, create, share, test and document APIs.
We will use Postman throughout the semester to make requests and receive responses from web server and APIs.

- Install and configure [postman](https://postman.com)
- Send HTTP requests to the test API [httpbin.org](http://httpbin.org/)
  - Send an **HTTP GET** request `http://httpbin.org/get`
  - Send an **HTTP POST** request `http://httpbin.org/post`
- Take a screenshot

## Submissions
Submit screenshots of your work. All lab submissions are due one hour before the following lab begins.


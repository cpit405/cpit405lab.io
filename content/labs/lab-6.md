---
title: "Lab 6"
date: 2024-10-13T18:42:06+03:00
draft: false
---

# Lab 06: JavaScript

## Objective

- learn how to handle events in JavaScript
- Work with the DOM API to find and modify elements
- learn how to use cookies


## Current Lab Learning Outcomes (LLO)

By completion of the lab the students should be able to

1. Implement client-side application logic using JavaScript
2. Handle event using JavaScript and DOM in client-side.
3. Utilize cookies to store data on the client

## Lab Requirements

Web Browser, Web Browser developer tools, Text Editor, and JavaScript reference.

## Lab Description

Create a web page that shows a like and dislike button with a counter to the number of likes and dislikes. Use cookies to prevent the user from voting twice. Additionally, allow users to leave comments along with their likes or dislikes. Use cookies to store the comments and prevent users from commenting multiple times. Allow users to reset their like/dislike choice, clear comments, and vote again. Use cookies to manage this reset functionality.

![like dislike example](/images/labs/lab-6/like-dislike-html-css-js-dom.png)


## Steps

1. **Create the HTML Page**
  - Add two buttons: one for "Like" and one for "Dislike".
  - Add counters next to each button to display the number of likes and dislikes.
  - Add a text input field and a submit button for comments.
  - Add a section to display the comments.
  - Add a reset button to allow users to reset their choices and comments.

2. **Set Up Initial JavaScript**
  - Initialize counters for likes and dislikes.
  - Initialize an array to store comments.
  - Check cookies for existing like/dislike choices and comments, and update the UI accordingly.

3. **Handle Like/Dislike Button Clicks**
  - Increment the like or dislike counter when the respective button is clicked.
  - Store the user's choice in cookies to prevent multiple votes.
  - Update the UI to reflect the new counts.

4. **Handle Comment Submission**
  - Capture the comment from the input field when the submit button is clicked.
  - Store the comment in cookies to prevent multiple comments.
  - Display the comment in the comments section.
  - Clear the input field after submission.

5. **Implement Reset Functionality**
  - Clear the cookies for the like/dislike choice and comments when the reset button is clicked.
  - Reset the counters and comments array.
  - Update the UI to reflect the reset state.

6. **Use Cookies for State Management**
  - Store the like/dislike choice in cookies.
  - Store the comments in cookies.
  - Retrieve and apply the stored state from cookies when the page loads.

7. **[Optional]: Enhance User Experience**
  - Add animations to the like and dislike buttons.
  - Display a confirmation message when the user resets their choices and comments.
  - Ensure the UI is responsive and works well on different devices.

### Submission

Push your code into GitHub with GitHub Pages enabled. Submit a link to your project hosted on GitHub Pages.

<span id="page2" class="anchor"></span>

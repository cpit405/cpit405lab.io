---
title: "Lab 7"
date: 2024-10-20T16:39:08+03:00
draft: false
---

# Asynchronous JavaScript (Ajax programming)
## XMLHTTPRequest, Fetch with Promises, and  Fetch with Async/Await
## Objective

- Consume an API using XHR, fetch, and async/await fetch
- Use the DOM API to create HTML elements dynamically
- Handle mouse and keyboard events in JS

## Lab Description

Create a web page that consumes the [unsplash API](https://unsplash.com/documentation).

![Unspalsh API](/images/labs/lab-7/unsplash-api-example.png)

## Steps

- **Step 1**: Sign up for an API key
  - Go to the [Unsplash Developers page](https://unsplash.com/developers).
  - Sign up or log in to create a new application.
  - Copy your Access Key.

- **Step 2**: Use Postman to test the API end point `/search`
  - Download and install Postman
  - Making API GET Request
    - Method:`GET`
    - URL: `https://api.unsplash.com/search/photos`
    - Headers: Key `Authorization` Value: `Client-ID YOUR_ACCESS_KEY`

- **Step 3**: Write an application using HTML, CSS, and JS to fetch images from the search API endpoint
  - Fetch from the API using XHR, fetch with promises, and fetch with async and await.

### Submission

Push your code into GitHub with a GitHub Pages enabled. Submit a link to your project hosted on GitHub Pages.

<span id="page2" class="anchor"></span>



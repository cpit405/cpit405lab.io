---
title: "Lab 4"
date: 2018-09-01T16:39:02+03:00
draft: false
---

# Lab 04: CSS Structure and Properties

Create an HTML form for placing an order to request auto parts and style it in CSS.

![](/images/lab-4.png)

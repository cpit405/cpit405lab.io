---
title: "Lab 9"
date: 2023-11-02T16:39:15+03:00
draft: false
---

# Lab 09: Working with APIs in React.js

![](/images/labs/lab-9/recipe-react-app.gif)

## Objective

- React Basics: Learn how to build a React app
- State management: Utilize the `useState` hook to manage the app's state
- Event handling: Implement event listeners to handle user interactions
- Routing: Employ React Router to implement navigation between different components
- React Hooks:
  - Use `fetch` with `useState` in response to user event.
  - Use `fetch` with `useEffect` when the component loads.


## Lab Description

Create a React application that allows users to search for recipes and view recipe details including ingredients, instructions, and images.

- You'll use a free API, [Spoonacular API](https://www.spoonacular.com/), to fetch recipes.
  - [Sign up for an account and generate a new api key](https://spoonacular.com/food-api/console).
    - Once you're logged in, navigate to the "Profile" section and view or generate an API key.
  - Read the docs on how to use the following API end points:
    - [api.spoonacular.com/recipes/complexSearch](https://spoonacular.com/food-api/docs#Search-Recipes-Complex)
    - [api.spoonacular.com/recipes/{id}/information](https://spoonacular.com/food-api/docs#Get-Recipe-Information)
  - Below is an example of using this end point. You may copy this into [Postman](https://www.postman.com/) and see the JSON response.
  
  ```shell
  https://api.spoonacular.com/recipes/complexSearch?apiKey=PASTE_YOUR_API_KEY_HERE&query=pasta
  
  https://api.spoonacular.com/recipes/716429/information?apiKey=PASTE_YOUR_API_KEY_HERE
  ```



### Submission

- Please submit a link to your project on CodeSandbox.io.
  - Create a project on [CodeSandbox.io](https://codesandbox.io/), be sure it is set to public, click on share, and copy link.

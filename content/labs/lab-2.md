---
title: "Lab 2"
date: 2023-09-10T18:10:20+03:00
draft: false
---

# Lab 02: Introduction, HTML Structure

## Objective

-   Practice HTML minimum structure and the different between document
    sections (header, and body).

-   Practice mostly used HTML tags.

-   Learn how to build HTML form, table, bullet/numbering/multilevel
    lists.

-   HTML layout design principle

## Current Lab Learning Outcomes (LLO)

By the endo this lab activity, you should be able to:
1.  Understand the basic structure of a web page.
2.  Use a number of HTML markup tags for building web pages
3. Publish and host a static web page.

## Lab Requirements

Code editor (e.g., VS Code), a web browser, git, and a free GitHub account.

## Lab Assessment

Consider the following starter HTML code:

 ```html
<!DOCTYPE html>  
<html>  
  <head>  
    <meta charset="utf-8">  
    <title>Hello, world!</title>  
  </head>  
  <body>  
    here is a body  
  </body>  
</html>
 ```

1.  Create an HTML document (e.g., lab2.html) with the following content:
  - The document title is "CPIT-405 lab 2" (`<title>`) and the page title is "Lab 2: HTML" (`<h1>`).
  -  Add a link (`<a>`) to “https://cpit405.gitlab.io”.
  - In the `<body>`, add two paragraphs (`<p>`) with any text you like. Please, separate these two tags with a horizontal rule tag (`hr`).
  - Add a clickable image (link (`<a>`) with image (`<img>`)). Use any image and any link you like and be sure to add an alternative text to the image.


2. create 5 paragraphs (`<p>`) of any text and use the following tags inside each paragraph:
  - `<strong>`
  - `<em>`
  - `<sup>`
  - `<sub>`
  - `<span>`


3. Add the following HTML headings to the HTML document. The number of #s below indicates of heading's level (e.g., `#` => `<h1>`, `##` => `<h2>`, etc.).

```
# Heading elements  
   
 ## Summary  
   
  Some text here...  
   
 ## Examples  
   
 ### Example 1  
   
  Some text here...  
   
 ### Example 2  
   
  Some text here...  
   
 ## See also  
   
  Some text here...  
    
```

4. Add the following lists to your HTML document:

   1. An ordered list `<ol>` with 3 items `<li>`.
   2. An unordered list `<ul>` with 3 items  `<li>`.
   3. A definition list `<dl>` with 2 pairs of term `<dt>` and description or definition `<dd>`

5. Create the table `<table>` below where the first row (`<tr>`) is the heading row (`<th>`):


| Basics   |      Front-end Frameworks      |  Server-side frameworks |
|----------|:-------------:|------:|
| HTML |  React | Spring |
| CSS |    Angular   |   Django |
| JS | Vue |    Laravel |


6. Create a form (`<form>`) with two inputs (`<input>`) for login and password) and a submit button (`<input>`).

## Submission
1. Create a new repository on GitHub.
2. Push your html page into the repository.
3. Enable GitHub pages for the repository.
4. Submit a link to the web page hosted on GitHub pages.

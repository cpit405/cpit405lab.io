---
title: "Lab 5"
date: 2024-10-06T16:39:04+03:00
draft: false
---

# Lab 05: DOM API and JS Events

## Description

In this lab, you will create an interactive NBA Player Stats Dashboard using JavaScript, the DOM API, and CSS. This project will demonstrate JS event types, dynamic element creation, and data manipulation.

## Learning Objectives:

By the end of this lab, students will be able to:

- Implement multiple types of JavaScript events (click, change, mouseover)
- Dynamically create and manipulate HTML elements using the DOM API
- Work with arrays and objects to manage and filter data
- Use event delegation for efficient event handling
- Implement basic data visualization using JavaScript
- Style elements dynamically using JavaScript and CSS.

![](/images/labs/lab-5.png)

## Steps

- Clone the starter code at [https://github.com/cpit405/lab-05](https://github.com/cpit405/lab-05).
- Implement the dashboard functionality in JavaScript:
  - Traverse the provided data as a JS object.
  - Populate the table with player data

- Add event listeners and handlers:
  - Search input events
  - Filter dropdown change events
  - Implement search and filter functionality
  - Dark mode toggle click event
  - Implement the dark mode toggle

- [OPTIONAL] Sorting Feature : Add table header click events for sorting


## Submission

Submit the link to the deployed web page on GitHub Pages.


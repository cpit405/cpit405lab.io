---
title: "Assignment 2 - CSS"
date: 2022-02-14T22:54:35+03:00
draft: false
---
<span class="tag is-info is-medium">Due date: Thursday 2/17/2022 at 11:59PM </span>

Style the three web pages you've implemented for assignment 1. The HTML of the web pages may be edited for styling purposes (e.g., adding classes, ids, or changing structure):
You will be graded on your creativity in designing the web pages.

The assignment should be stored in a repository on GitHub and hosted on [GitHub Pages](https://pages.github.com/) as a project site.  The repo may be named `cpit405-assignment-2`, so the URL looks like: `username.github.io/cpit405-assignment-2`.

### Submission

Please submit the links to both your repository on GitHub and the hosted site on GitHub pages as indicated by your instructor.
---
title: "Assignment 5"
date: 2018-11-28T18:06:53+03:00
draft: true
---

## Assignment 5: PHP/MySQL
<span class="tag is-info is-medium">Due date: Sunday 16/12/2018 at 12:00PM </span>

You are asked to store a set of images in a MySQL database and fetch the images dynamically using PHP. You will query the database and obtain a list of images in your PHP code, which generates the HTML code needed to show the images in a single web page.

### Notes:
- You should use an online IDE such as [c9.io](https://c9.io) to write and run your program.
- **Optional extra credit**: Deploy the app on [Heroku](https://www.heroku.com/).
- The assignment should be stored in a repository on GitHub. The repo should be named `cpit405-assignment-5`.

### Submission
Please submit the links to your GitHub repository in private to the instructor on Slack.

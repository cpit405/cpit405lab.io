---
title: "Assignment 4"
date: 2018-08-25T22:54:40+03:00
draft: true
---
## Assignment 4: AJAX/JSON
<span class="tag is-info is-medium">Due date: Sunday 18/11/2018 at 12:00PM </span>

You are asked to use the API of the service you chose to fetch images dynamically using AJAX. You will query the API and obtain a list of images in JavaScript and incorporate the same some sort criteria you implemented in the previous assignment.

### Notes:

- If the API of the service you've chosen is complex ore requires a complicated authentication mechanism, then you may use a simpler API such as the [imgur API](https://apidocs.imgur.com/).
- The sort function has to be done at the client side (i.e. no server side code).

The assignment should be stored in a repository on GitHub and hosted on [GitHub Pages](https://pages.github.com/) as a project site.  The repo may be named `cpit405-assignment-4`, so the URL looks like: `username.gitlab.io/cpit405-assignment-4`.

### Submission

Please submit the links to both your repository on GitHub and the hosted site on GitHub pages in private to the instructor on Slack.
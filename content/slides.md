---
title: "Lecture Slides"
date: 2024-03-07T15:08:42+03:00
draft: false
---

All lecture slides with interactive code editors are listed here. If you wish to download them as a PDF, Please click on the lecture slides link, hover on the top left corner, and click on the download icon. Please note that using a PDF file instead of live hosted slides is not recommended as it won't get updated when the instructor makes changes to the lecture slides.

## Intro Slides
- [Intro Slides](https://cpit405.github.io/web-slides/) 

## HTML Slides
- [HTML Slides](https://cpit405.github.io/html-slides/)

## CSS Slides 
- [CSS Slides](https://cpit405.github.io/css-slides/)

## JavaScript Slides
- [JS Slides](https://cpit405.github.io/js-slides/)

## React Slides
- [React Slides](https://cpit405.github.io/react-slides/)

## PHP Slides
- [PHP Slides](https://cpit405.github.io/php-slides/)

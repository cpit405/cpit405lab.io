---
title: "Project"
date: 2024-10-09T09:17:18+03:00
draft: false
---

### Project Overview

Form a team consisting of 2-3 students to develop a web application using React. Your web app should solve a specific problem for a particular group of users. At minimum, your web app should include the following features:

1. ~~**User Authentication**: Utilize a service that manages user login and registration.~~ *(Optional in Fall 2024, required for future semesters)*
2. ~~**Protected Routes**: Ensure that certain routes within the application are accessible only to authenticated users.~~ *(Optional in Fall 2024, required for future semesters)*
3. **API Integration**: The application should interact with an external API to fetch or send data.

### Proposal Submission
Please submit a PDF document that includes the following information:

1. **Project Idea**: Provide a detailed description of the problem your web application aims to solve and how it will address this problem.
2. **Paper Prototype**: Submit a low-fi/paper prototype of your app. The prototype should clearly illustrate the user interface and user experience of your application, including key screens, navigation flow, and major functionalities. This will help in visualizing the design and layout before moving on to the development phase.
3. **Team Members**: List the names and roles of your team members.

## Project Submission
Please submit a link to your project on GitHub. Please make sure the repository reflects the contributions of all group members. You will need to demonstrate the project during the last lecture of the semester.

### Grading Rubric

| Criteria | Excellent [2 marks] | Fair [1 mark] | Poor [0 marks] |
|----------|----------------------|---------------|----------------|
| **1. HTML** | Well-structured and semantic HTML | Basic HTML structure with minor issues | Poorly structured HTML |
| **2. CSS [^1]** | Responsive and well-styled | Basic styling with some responsiveness | Poor or no styling |
| **3. JavaScript Interactivity and Form Validation** | Interactive UI and proper form validation | Basic interactivity, missing or minor issues in form validation | Poor or no interactivity and form validation |
**4. User Events** | Efficient handling of mouse and keyboard events | Basic handling of user events with minor issues | Poor or no handling of user events |
| **5. React Components** | Well-organized and reusable components | Basic components with some reusability | Poorly organized components |
| **6. Prop Management** | Proper passing and management of props | Basic prop management with minor issues | Poor or no prop management |
| **7. State Management** | Proper use of state management techniques | Basic state management with minor issues | Poor or no state management |
| **8. Routing and Navigation** | Smooth navigation across multiple pages | Basic navigation with minor issues | Poor or no navigation |
| **9. API Fetching** | Efficient and error-handled API calls | Basic API calls with minor issues | Poor or no API integration |
| **10. Key Features** | All specified features are fully functional | Most features are functional with minor issues | Few or no features are functional |
| **11. Deployment** | Successfully deployed on GitHub pages or similar | Deployed with minor issues | Not deployed |
| **12. Documentation** | README.md file with description, build instructions, and screenshots of the React app | Basic README.md file with minor issues in description, build instructions, or screenshots | Poor or no README.md file |
| **13. Accessibility [^2]** |  | Fully accessible using semantic HTML tags and alt text for images | Poor or no accessibility |
| **Total Marks** | **25** 
__________

[^1]: The use of CSS frameworks is allowed
[^2]: Accessibility is worth 1 mark.





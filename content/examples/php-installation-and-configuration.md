---
title: "Install and Configure PHP"
date: 2022-04-11T18:21:50+03:00
draft: false
description: "Instructions on how to install and configure PHP."
---

PHP is mainly focused on server-side scripting, so we need to install it on the server with a working PHP installation. While there are many ways to install PHP in a complete packaged stack, this lecture note focouses on installing PHP alone and running it from the command line with the built-in web server.

## Installing PHP on Windows
- [Download the appropriate zip file for your architecture](https://windows.php.net/download).
- Extract the ZIP file.
- Add the extracted directory to the *PATH* environment variable.
- Start the *Command Prompt*/*CMD* application and run `php -v'

Below is a video on how PHP can be installed on Windows 11 or 10, configuring the environment variable, and running PHP from the *Command Prompt (CMD)*.

{{< youtube l-74L_8L3CU >}}


## Installing PHP on macOS
The easiest way to install PHP on macOS is to use *brew*, the package manager.
You can install homebrew, by following the instructions at [brew.sh](https://brew.sh/). Next, run: 

`brew install php`.

## Installing PHP on Linux
Installing PHP on Linux depends on the Linux distribution you are using. For instance on a debian based Linux system, you may use `apt` to install PHP:

`apt install php-common php-cli`.

On an RPM-based distributions such as Red Hat Linux or CentOS/Rocky Linux, you can *yum*:

`yum install php`


## Running PHP from the command line (CLI)
- Open your terminal app (e.g., command prompt on Windows or Terminal app on macOS):
- Go to the directory/folder where your PHP project is stored at: `cd /path/to/project/`.
- Run the built-in web server using: `php -S -t .`, where `.` refers to the current working directory you cd'ed to in the previous step.
---
title: "Mouse and Keyboard Events in JavaScript"
description: "This lecture note discusses how to register and handle mouse and keyboard events."
date: 2022-02-19T01:23:10+03:00
draft: false
---

## Mouse Events
Mouse events are triggered when using a pointer device like the mouse. The most common events are: `click`, `dbclick`, `mouseup` and `mousedown`. There are two approaches for registering mouse or any event handlers in general: 
1. inline in the HTML using the **onevent property** (e.g. `onclick` for the click event). 
2. Dynamically in JS by adding the event handler to the element in the DOM tree using the `addEventListener()` method.

### 1) Registering Mouse Events Inline (in HTML)
We can add mouse events inline in HTML using the _onevent_ property. For example, the click event can be registered using the `onclick` property and assigning it to the a function **with parentheses for invocation** as shown in the following example:

{{< jsfiddle userId="kalharbi" fiddleId="9pcLqkrm" tabs="html,result" dark="true" >}}


### 2) Registering Mouse Events in the DOM (in JS)
We can register mouse events in _JavaScript_ using `addEventListener` of the DOM element and assigning it to the function name ** without parentheses** so it will be invoked when the event is fired inside the _addEventListener_ function as in the following example:

{{< jsfiddle userId="kalharbi" fiddleId="hov85wq7" tabs="js,html,result" dark="true" >}}


## Keyboard events
Keyboard events are triggered when interacting with the keyboard. There are two keyboard events: `keydown` (a key has been pressed) and `keyup` (a key has been released). Similar to mouse events, there are two approaches for registering keyboard or any event handlers in general:
1. Inline in the HTML using the **onevent property** (e.g. `onkeydown` for pressing a key and `onkeyup` for releasing a key).
2. Dynamically in JS by adding the event handler to the element in the DOM tree using the `addEventListener()` method.

### 1) Registering Keyboard Events Inline (in HTML)
We can add keyboard events inline in HTML using the _onevent_ property. For example, the *key up* event can be registered using the `onkeyup` property and assigning it to the a function **with parentheses for invocation**. The following example uses the `onkeyup` event to listen to key press and release events to convert numbers entered in Arabic numerals to English numerals.

{{< jsfiddle userId="kalharbi" fiddleId="jyk923bd" tabs="html,js,result" dark="true" >}}

### 2) Registering Keyboard Events in the DOM (in JS)
Similar to registering mouse events, we can register keyboard events in _JavaScript_ using `addEventListener` of the DOM element and assigning it to the function name **without parentheses** so it will be invoked when the event is fired inside the _addEventListener_ function as in the following example:

{{< jsfiddle userId="kalharbi" fiddleId="84bfsotz" tabs="html,js,result" dark="true" >}}

### Removing Registered Mouse or Keyboard Events

We can remove registered mouse or keyboard events using `removeEventListener` as shown in the following example:

{{< jsfiddle userId="kalharbi" fiddleId="m7qwo0gu" tabs="js,html,result" dark="true" >}}

## Complete Example
The following example shows how to filter a table by a word and/or by an item in a drop down menu.

{{< jsfiddle userId="kalharbi" fiddleId="pwkLtmvc" tabs="result,html,js,css" dark="true">}}


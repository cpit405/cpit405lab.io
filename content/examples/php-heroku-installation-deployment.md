---
title: "Deploy a PHP app on Heroku"
date: 2022-05-16T19:24:31+03:00
draft: false
description: "Instructions on how to install heroku and deploy a simple PHP application."
---

# Heroku Platform

Heroku is a popular Platform as a Service (PaaS) for deploying applications. It features a generous free plan and provides developer-friendly tools to build and run web applications.

You will use heroku to build and deploy your PHP applications throughout the last part of this course. Refer to the video below on how install and set up your local development environment to build and deploy PHP applications on Heroku. I will go through the required steps to install the Heroku CLI and the tools that Heroku depends upon. At the end of the video, you will have Heroku installed and deploy a simple PHP application.

## Installing Heroku
- [Install Git](https://git-scm.com/book/en/v2/Getting-Started-Installing-Git)
- [Install the Heroku CLI](https://devcenter.heroku.com/articles/heroku-cli#install-the-heroku-cli)
- [Install Composer](https://getcomposer.org/)
- [Install and configure PHP as per the previous instructions](../php-installation-and-configuration)


{{< youtube -PD_KZbTrww >}}